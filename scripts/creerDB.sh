#!/bin/bash

# création du dossier de résultat
mkdir -p bin

# on transforme le pdf en txt
pdftotext -layout Femme\,\ j\'écris\ ton\ nom...\ \:\ guide\ d\'aide\ à\ la\ féminisation\ des\ noms\ de\ métiers\,\ titres\,\ grades\ et\ fonctions.pdf bin/guide.txt

# transforme le guide au format JSON
node scripts/jsonify.js > bin/guide.json

# on fait le parsing !
node scripts/parser.js > bin/data.json

# maintenant on créer les règles
node scripts/correcteur.js > bin/règles.json
