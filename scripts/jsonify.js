fs = require('fs')

function read(cb)
{
  fs.readFile('bin/guide.txt', 'utf8', function (err,data)
  {
    if (err)
    {
      return console.log(err)
    }
    cb(data)
  })
}

function parse()
{

  read(function(data)
  {
    console.log(JSON.stringify(data))
  })
}


parse()
