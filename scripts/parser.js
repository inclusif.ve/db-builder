fs = require('fs')

function read(cb)
{
  fs.readFile('bin/guide.txt', 'utf8', function (err,data)
  {
    if (err)
    {
      return console.log(err)
    }
    cb(data)
  })
}

function isListStart(data)
{
  return (data.trim() === 'A')
}
function isListEnd(data)
{
  return (data.trim().includes('Liste annexe'))
}

function clean(data)
{
  var dataTmp = data.trim()
  if (dataTmp.length < 2)
  {
    return null
  }
  if (dataTmp.startsWith("Index"))
  {
    return null
  }
  if (dataTmp.match(/^[0-9]+$/) != null)
  {
    return null
  }
  if (dataTmp.startsWith("le, un"))
  {
    return null
  }

  if (dataTmp.includes(' voir ') || dataTmp.includes('[') || dataTmp.includes(';') || dataTmp.includes('avec ') || dataTmp.includes('souvent') || dataTmp.includes(' et ') || dataTmp.includes('XIX') || dataTmp.includes(' plus') || dataTmp.includes('mais ') || dataTmp.includes('dans ') || dataTmp.includes('ecommand') || dataTmp.includes('rare') || dataTmp.includes('attest') || dataTmp.includes('»')  || dataTmp.includes('sens')  || dataTmp.includes('masculin') || dataTmp.includes('Moyen Âge')|| dataTmp.includes('conforme') || dataTmp.includes('envisageable') || dataTmp.includes('catholiques') || dataTmp.includes('verbe'))
  {
    return null
  }
  return dataTmp
}

function cleanLineArray(data)
{
  var dataTmp = data
  var i = 0
  while (i < dataTmp.length)
  {
    if (dataTmp[i].length === 0 || dataTmp[i].match(/^[0-9]+$/) != null)
    {
      dataTmp.splice(i, 1)
      i--;
    }
    i++
  }
  return dataTmp
}

function getWordType(word)
{
  if (word.startsWith('('))
  {
    if (word.includes(')'))
    {
      return 'indication'
    }
    else
    {
        return 'start indication'
    }
  }

  if (word.endsWith(')'))
  {
      return 'end indication'
  }

  if (word.startsWith('*'))
  {
    return 'asterix' //est là!
  }
  return 'word'
}

function parseLine(line)
{
  var data = cleanLineArray(line.split(/[ ]{2}/))

var res = []
var str = ""
var pause = false
var size = 0
  for (var i = 0; i < data.length; i++)
  {
    data[i] = data[i].trim()
    var type = getWordType(data[i])
    if (type === 'asterix')
    {
      break
    }
    if (type === 'start indication')
    {
      pause = true
    }
    if (pause && type === 'end indication')
    {
      pause = false
    }
    else if (type === 'end indication')
    {
      type = 'word'
    }
    if (type === 'word' && !pause && size < 2 && data[i].length > 2)
    {
      data[i] = data[i].replace(/ *\([^)]*\) */g, "").trim()
      if (data[i].endsWith('*'))
      {
        data[i] = data[i].slice(0, data[i].length - 1)
      }
      if (data[i].endsWith(','))
      {
        data[i] = data[i].slice(0, data[i].length - 1)
      }
      if (data[i].endsWith('*'))
      {
        data[i] = data[i].slice(0, data[i].length - 1)
      }

      if (data[i].includes(','))
      {
        //désolé on ne sais pas encore faire !
        return null
      }
      size++
      res[res.length] = data[i]
      str += data[i] + "-"
    }
  }
  if (str.length > 0 && size == 2)
  {
    return res;
  }

  return null

}

function parse()
{

var M2W = {}
var W2M = {}

  read(function(data)
  {
    var dataLine = data.split('\n')
    var isStarted = false
    for (var i = 0; i < dataLine.length; i++)
    {
      if (!isStarted)
      {
        isStarted = isListStart(dataLine[i])
      }
      else
      {
        if (isListEnd(dataLine[i]))
        {
          break
        }
        var dataClean = clean(dataLine[i])
        if (dataClean)
        {
          var couple = parseLine(dataClean)
          if (couple)
          {
            M2W[couple[0]] = couple[1]
            W2M[couple[1]] = couple[0]
          }
        }
      }
    }
    var total = {M2W: M2W, W2M: W2M}
    console.log(JSON.stringify(total))

  })
}


parse()
