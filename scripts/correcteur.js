fs = require('fs')

function read(cb)
{
  fs.readFile('bin/data.json', 'utf8', function (err,data)
  {
    if (err)
    {
      return console.log(err)
    }
    cb(data)
  })
}



function createInclusiveWithCouple(m, f)
{

if (m.includes("-") && m !== f)
{
  if (m < f)
  {
    return m + "·" + f
  }
  return f + "·" + m
}

  var separation = 0

  while (m[separation] && f[separation] && m[separation] === f[separation])
  {
    separation++
  }

  if (separation === m.length && separation === f.length)
  {
    return m
  }

  var commun = m.slice(0, separation)
  var onlyM = m.slice(separation, m.length)
  var onlyF = f.slice(separation, f.length)

  var pos1 = null
  var pos2 = null

  if (onlyM.length < onlyF.length)
  {
    pos1 = onlyM
    pos2 = onlyF
  }
  else if (onlyM.length > onlyF.length)
  {
    pos1 = onlyF
    pos2 = onlyM
  }
  else  if (onlyM > onlyF)
  {
    pos1 = onlyM
    pos2 = onlyF
  }
  else
   {
     pos1 = onlyF
     pos2 = onlyM
  }
  var letter = ""
  if (commun && commun.length > 0)
  {
    letter = commun[commun.length-1]
  }

  return commun + pos1 + "·" + letter + pos2
}

function parse()
{
  var rules = {}
  read(function(data)
  {
    var dataParsed = JSON.parse(data)
    for (var couple in dataParsed.M2W)
    {
      var res = createInclusiveWithCouple(couple, dataParsed.M2W[couple])
      rules[couple] = res
        rules[dataParsed.M2W[couple]] = res
    }

    console.log(JSON.stringify(rules))
  })
}


parse()
